from selenium import webdriver
import logging
import sqlite3
import re

class suche:

    def __init__(self, path):
        self.data = sqlite3.connect('wanted.db')
        self.data.executescript('''
            CREATE TABLE IF NOT EXISTS people (
                id TEXT PRIMARY KEY,
                description TEXT NOT NULL
            );
        ''')
        self.data.commit()

        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        self.driver = webdriver.Chrome(options=options)

        self.poll_pages(path)


    def __del__(self):
        self.driver.quit()


    def poll_pages(self, path):
        logging.info('pages from %s', path)
        self.driver.get(path)

        for page in {page.get_attribute('href') for page in self.driver.find_elements_by_class_name('page-link')}:
            self.poll_faces(page)


    def poll_faces(self, path):
        logging.info('faces from %s', path)
        self.driver.get(path)

        seen = set()
        faces = {l.get_attribute('href') for l in self.driver.find_elements_by_xpath('//a[@href]')}
        for link in faces:
            if link not in seen and re.search('appeal\?ID', link):
                self.poll_achievements(link)
            seen.add(link)


    def poll_achievements(self, path):
        logging.info('info for %s', path)
        self.driver.get(path)

        photo = self.driver.find_element_by_xpath('//figure/img')
        guilty = {'Photo': photo.get_attribute('src')}

        desc = self.driver.find_element_by_xpath('//div[@class="col-md-11 col-lg-8"]')

        m = re.search('Summary\n(?P<Summary>(.*\n)*)'
                      'Full Details(?P<Detail>(.*\n)*)'
                      'Suspect description', desc.text, re.MULTILINE)

        for k, v in m.groupdict().items():
            guilty[k] = v.strip()

        for info in desc.find_elements_by_tag_name('li'):
            k, v = info.text.split(':')
            guilty[k.strip()] = v.strip()

        self.data.execute('''
            INSERT INTO people VALUES (?, ?)
        ''', (path.split('=')[1], str(guilty)))
        self.data.commit()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s: %(message)s', level=logging.INFO)
    suche('https://crimestoppers-uk.org/give-information/most-wanted')

